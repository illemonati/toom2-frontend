import { Session } from "@/session";
import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        socket: null as null | WebSocket,
        socketConfig: "ws://localhost:2430",
        session: null as Session | null
    },
    mutations: {
        setSocket(state, socket: WebSocket | null) {
            state.socket = socket;
        },
        setSession(state, session: Session | null) {
            state.session = session;
        }
    },
    actions: {},
    modules: {}
});
